package com.sivarajan931.eazy_home_admin_client


import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.animation.OvershootInterpolator
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.json.JSONObject


class MainScreen : AppCompatActivity() {

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main__screen)
        actionBar?.hide()
        var jsonData: JSONObject? = null
        var auto = true
        val translationY = 100f
        val mQueue = Volley.newRequestQueue(this@MainScreen)
        val toggleSwitch = findViewById<Switch>(R.id.myToggleSwitch)
        val deviceSelector: Spinner = findViewById(R.id.spinner)
        val deviceIdHolder: TextView = findViewById(R.id.textView2)
        val fab1 = findViewById<FloatingActionButton>(R.id.floatingActionButton)
        val fab2 = findViewById<FloatingActionButton>(R.id.floatingActionButton2)
        val menuButton = findViewById<FloatingActionButton>(R.id.menuButton);
        val fab3 = findViewById<FloatingActionButton>(R.id.floatingActionButton3)
        val fab4 = findViewById<FloatingActionButton>(R.id.floatingActionButton4)
        val imageView = findViewById<ImageView>(R.id.imageView2)
        var menuFlag = false
        val interpolator = OvershootInterpolator()

        menuButton.animate().setInterpolator(interpolator).rotation(0f).setDuration(300).start();
        fab1.animate().translationY(translationY).alpha(0f).setInterpolator(interpolator)
            .setDuration(300).start()
        fab2.animate().translationY(translationY).alpha(0f).setInterpolator(interpolator)
            .setDuration(300).start()
        fab3.animate().translationY(translationY).alpha(0f).setInterpolator(interpolator)
            .setDuration(300).start()
        fab4.animate().translationY(translationY).alpha(0f).setInterpolator(interpolator)
            .setDuration(300).start()
        menuButton.setOnClickListener {
            if (!menuFlag) {
                menuFlag = true;


                menuButton.animate().setInterpolator(interpolator).rotation(45f).setDuration(300)
                    .start()

                fab1.animate().translationY(0f).alpha(1f).setInterpolator(interpolator)
                    .setDuration(300).start()
                fab2.animate().translationY(0f).alpha(1f).setInterpolator(interpolator)
                    .setDuration(300).start()
                fab3.animate().translationY(0f).alpha(1f).setInterpolator(interpolator)
                    .setDuration(300).start()
                fab4.animate().translationY(0f).alpha(1f).setInterpolator(interpolator)
                    .setDuration(300).start()
            } else {
                menuFlag = false;

                menuButton.animate().setInterpolator(interpolator).rotation(0f).setDuration(300)
                    .start();
                fab1.animate().translationY(translationY).alpha(0f).setInterpolator(interpolator)
                    .setDuration(300).start()
                fab2.animate().translationY(translationY).alpha(0f).setInterpolator(interpolator)
                    .setDuration(300).start()
                fab3.animate().translationY(translationY).alpha(0f).setInterpolator(interpolator)
                    .setDuration(300).start()
                fab4.animate().translationY(translationY).alpha(0f).setInterpolator(interpolator)
                    .setDuration(300).start()
            }
        }


        val deviceList = ArrayList<String>()

        toggleSwitch?.setOnCheckedChangeListener { _, isChecked ->
            if (!auto) {
                val state = if (isChecked) "on" else "off"
                val mySettings = SharedPreference(this)
                var devIP: String?
                if (mySettings.getValueString("ip") != null)
                    devIP = mySettings.getValueString("ip")
                else devIP = "192.168.0.100"
                val prefix = if (mySettings.getValueString("prefix") != null)
                    mySettings.getValueString("prefix")
                else "http"
                val url = "$prefix://$devIP/api/device/" + deviceIdHolder.text + "/" + state
                val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,
                    { response ->
                        jsonData = response
                        Toast.makeText(this@MainScreen, "Refreshing data..", Toast.LENGTH_LONG)
                            .show()
                        val keys: MutableIterator<String> = response.keys()
                        var nextElement: String = keys.next()

                        if (nextElement != "Error") {
                            deviceList.clear()
                            deviceList.add(nextElement)
                            do {
                                nextElement = keys.next()
                                deviceList.add(nextElement)
                            } while (keys.hasNext())
                            val arrayAdapter = ArrayAdapter<String>(
                                this@MainScreen,
                                android.R.layout.simple_spinner_dropdown_item,
                                deviceList
                            )
                            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            deviceSelector.adapter = arrayAdapter
                            if (state == "on")
                                imageView.setImageResource(R.drawable.ic_lightbulb_on_black_48dp)
                            else
                                imageView.setImageResource(R.drawable.ic_lightbulb_outline_black_48dp)
                        } else {
                            Toast.makeText(this@MainScreen, "Error", Toast.LENGTH_SHORT).show()
                        }
                    },
                    { error ->
                        // TODO: Handle error
                        Toast.makeText(this@MainScreen, "Internal Error: $error", Toast.LENGTH_LONG)
                            .show()
                    }
                )
                mQueue.add(jsonObjectRequest)

                // Toast.makeText(this@MainScreen, msg, Toast.LENGTH_SHORT).show()
            }
        }
        deviceSelector.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val sel = deviceList[position]
                val state: String
                if (jsonData != null) {
                    state = jsonData!!.getString(sel)
                    deviceIdHolder.text = sel
                    auto = true
                    toggleSwitch.isChecked = (state == "on")
                    auto = false
                    if (state == "on")
                        imageView.setImageResource(R.drawable.ic_lightbulb_on_black_24dp)
                    else
                        imageView.setImageResource(R.drawable.ic_lightbulb_outline_black_24dp)

                } else
                    Toast.makeText(this@MainScreen, "Error: Invalid Selection.", Toast.LENGTH_SHORT)
                        .show()

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                Toast.makeText(this@MainScreen, "Select Something", Toast.LENGTH_SHORT).show()
            }
        }
        fab4.setOnClickListener {
            val mySettings = SharedPreference(this)

            val devIP: String = if (mySettings.getValueString("ip") != null)
                mySettings.getValueString("ip")!!
            else "192.168.0.100"
            val prefix = if (mySettings.getValueString("prefix") != null)
                mySettings.getValueString("prefix")
            else "http"
            val url = "$prefix://$devIP/api/status"
            val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,
                { response ->
                    jsonData = response
                    Toast.makeText(this@MainScreen, "Refreshing data..", Toast.LENGTH_LONG).show()
                    val keys: MutableIterator<String> = response.keys()
                    var nextElement: String = keys.next()

                    if (nextElement != "Error") {
                        deviceList.clear()
                        deviceList.add(nextElement)
                        do {
                            nextElement = keys.next()
                            deviceList.add(nextElement)
                        } while (keys.hasNext())
                        val arrayAdapter = ArrayAdapter<String>(
                            this@MainScreen,
                            android.R.layout.simple_spinner_dropdown_item,
                            deviceList
                        )
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        deviceSelector.adapter = arrayAdapter
                    } else {
                        Toast.makeText(this@MainScreen, "Error", Toast.LENGTH_SHORT).show()
                    }
                },
                { error ->
                    // TODO: Handle error
                    Toast.makeText(this@MainScreen, "Internal Error: $error", Toast.LENGTH_LONG)
                        .show()
                }
            )
            mQueue.add(jsonObjectRequest)


        }

        Handler().postDelayed({
            fab4.performClick()
        }, 1000 * 5 * 60) // 3000 is the delayed time in milliseconds.


        fab1.setOnClickListener {
            val intent = Intent(this@MainScreen, FaceVerificationClient::class.java)
            startActivity(intent)

        }
        fab2.setOnClickListener {
            val intent = Intent(this@MainScreen, Settings::class.java)
            startActivity(intent)

        }
        fab3.setOnClickListener {
            val mySettings = SharedPreference(this)
            val devIP = if (mySettings.getValueString("ip") != null)
                mySettings.getValueString("ip")
            else "192.168.0.100"
            val prefix = if (mySettings.getValueString("prefix") != null)
                mySettings.getValueString("prefix")
            else "http"
            val url = "$prefix://$devIP/plot_graph"
            val openURL = Intent(android.content.Intent.ACTION_VIEW)
            openURL.data = Uri.parse(url)
            startActivity(Intent.createChooser(openURL, "GraphData"));

        }
        fab1.setOnLongClickListener {

            Toast.makeText(this@MainScreen, "Unlock Door using Face recognition", Toast.LENGTH_LONG).show()
            return@setOnLongClickListener true
        }
        fab2.setOnLongClickListener {
            Toast.makeText(this@MainScreen, "Settings", Toast.LENGTH_LONG).show()
            return@setOnLongClickListener true
        }
        fab3.setOnLongClickListener {
            Toast.makeText(this@MainScreen, "Shows the appliace usage on a web browser", Toast.LENGTH_LONG).show()
            return@setOnLongClickListener true
        }
        fab4.setOnLongClickListener {
            Toast.makeText(this@MainScreen, "Refresh this page", Toast.LENGTH_LONG).show()
            return@setOnLongClickListener true
        }
        menuButton.setOnLongClickListener {
            Toast.makeText(this@MainScreen, "This is menu Button.", Toast.LENGTH_LONG).show()
            return@setOnLongClickListener true
        }
    }
}







