'''
A module to progress regresion to provide user data
'''
from sklearn import linear_model
from matplotlib import pyplot as plt
import matplotlib
import numpy as np
import os
import yaml
import pandas as pd
from datetime import datetime as dt
from datetime import timedelta
import sys


def get_labels(directory='./static/data'):
    x_values = []
    y_values = []
    x_label = []
    inc = 30
    all_names = []
    all_date_objects = []
    splitStr = []

    for root, dirs, files in os.walk(directory, topdown=False):
        for name in files:
            if name.endswith('.yaml'):
                all_names.append(name)

    for name in all_names:
        splitStr = name.split('-')
        splitStr[1] = splitStr[1].split('.')[0]
        actual_date = dt(int(splitStr[1]), int(splitStr[0]), 28)
        all_date_objects.append(actual_date)

    all_date_objects = sorted(all_date_objects)

    for date in all_date_objects:
        string = date.strftime("%m-%Y")
        x_label.append(string)

    for name in x_label:
        try:
            if os.path.exists(f'{directory}/{name}.yaml'):
                with open(f'{directory}/{name}.yaml', 'r') as f:
                    data = yaml.load(f, Loader=yaml.FullLoader)
                y_values.append(int(data.pop("total-usage", 0)))
                x_values.append(inc)
                inc += 30
        except Exception as e:
            print(e)

    return x_values, y_values, x_label


def get_predictions(value):
    x_values, y_values, _lables = get_labels()
    x = merge_array(x_values, y_values)
    dataframe = pd.DataFrame(x, columns=['days', 'hours'])
    x_values = dataframe['days'].values[:, np.newaxis]
    y_values = dataframe['hours'].values[:, np.newaxis]
    body_reg = linear_model.LinearRegression()
    body_reg.fit(x_values, y_values)
    predict_values = np.array(value, dtype=np.float64).reshape(1, -1)
    real_predictions = body_reg.predict(predict_values)
    print(f'The predicted values are: {real_predictions}')
    return real_predictions


def draw_regression(v, Show=False):
    if v > 0:
        x_values, y_values, x_label = get_labels()
        x_values.append(v)
        old_x = x_values
        if v - x_values[len(x_values) - 1] < 365:
            my_preditions = get_predictions([v])[0][0]
            y_values.append(int(my_preditions))
            old_y = y_values
            new_x = [x_values[len(x_values) - 2]]
            new_y = [y_values[len(y_values) - 2]]
            new_x_label = [x_label[len(x_label) - 1]]
            string = x_label[len(x_label) - 1]
            splitStr = string.split('-')
            splitStr[1] = splitStr[1].split('.')[0]
            actual_date = dt(int(splitStr[1]), int(splitStr[0]), 28)
            predictedMonthName = actual_date + timedelta(days=(v - x_values[len(x_values) - 2]))
            print(v - x_values[len(x_values) - 1])
            predictedMonthName = predictedMonthName.strftime("%m-%Y")
            x_label.append(predictedMonthName)
            x = merge_array(x_values, y_values)
            dataframe = pd.DataFrame(x, columns=['days', 'hours'])
            x_values = dataframe['days'].values[:, np.newaxis]
            y_values = dataframe['hours'].values[:, np.newaxis]
            body_reg = linear_model.LinearRegression()
            body_reg.fit(x_values, y_values)
            plt.scatter(x_values, y_values)
            plt.plot(x_values, y_values, 'bo', "#049", linewidth=4, markersize=6, linestyle="solid")
            new_x.append(v)
            new_y.append(my_preditions)
            plt.scatter(new_x, new_y)
            plt.plot(new_x, new_y, "#049", linewidth=2, markersize=3, linestyle="dashed")
            plt.xlabel('days')
            plt.ylabel('total hours of usage')
            plt.title("Your overall applaince usage statistics")
            if not os.path.exists('./static/graphs/plot_regression'):
                os.mkdir('./static/graphs/plot_regression')
            plt.savefig("./static/graphs/plot_regression/linearreg.png")
            if Show:
                plt.show()
            # m = merge_array(old_x, x_label)
            # TODO: MERGE THE old_x,x_label,old_y
            mylist = []
            for i in range(len(old_x)):
                mylist.append((old_x[i], x_label[i], old_y[i]))
            return mylist, old_x[len(old_x) - 1], x_label[len(old_x) - 1]
        else:
            print("Value cannot be zero/null")
            raise ValueError()


def merge_array(x_values, y_values):
    merged_array = list(zip(x_values, y_values))
    for i in range(len(merged_array)):
        merged_array[i] = list(merged_array[i])
    return merged_array


def showRegressionFor30days():
    x_values, _y, _xl = get_labels()
    v = x_values[(len(x_values) - 1)] + 30
    print(x_values[(len(x_values) - 1)] + 30)
    return draw_regression(v, False)


if __name__ == "__main__":
    try:
        v = int(sys.argv[1])
        print(v)
        draw_regression(v, True)

    except:
        showRegressionFor30days()
