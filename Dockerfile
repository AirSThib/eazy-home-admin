FROM debian
RUN apt update
RUN apt-get install 'ffmpeg'\
    'libsm6'\ 
    'libxext6'  -y
RUN apt install -y openssl neofetch python autoconf pkgconf gcc flex patch make  m4 cmake python3-pip automake libatlas-base-dev libgtk-3-dev libboost-all-dev
WORKDIR /usr/src/app
COPY . .
RUN ls
RUN pip3 install -r requirements.txt
EXPOSE 5000
CMD ["python3","./init.py"]