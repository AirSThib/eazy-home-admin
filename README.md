[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/sivarajan931/eazy-home-admin)

# Eazy Home Admin
This project is done by sivarajan under GPLv3 License.he gitlab repository link is: https://www.gitlab.com/sivarajan931/eazy-home-admin. 

# Credits
  Airsthib - Desgin

## Description
### Abstract
Home automation, home control, smart or digital home are just different names for comfort, convenience, security and power saving. These systems are of increasing importance these days. Even though such systems are very expensive in general, they can also be very economical   if one design and construct them for very specific needs. Some existing projects like Google Home and Amazon Alexa comes with data privacy or data theft allegations, however our project will aim to ensure that the personal data stays within the user control while providing the capabilities of home automation. Our project also features with Artificial Intelligence based security systems. 

Basically there are two modules in our solution, namely client and the server. The server will be able to run on your home PC or a cheap devices like Raspberry PI. The client is mobile app which is installed on an android smart phone. The app consist of user interface using which, user will be able to control home appliances via any Android Smartphone. On the server side there will be a dedicated Arduino board connected to the PC/Raspberry PI using USB cable to control the home appliances. The server-side module operates on Local Area Network with the required ports opened to connect with the client. So LAN or Wi-Fi is a must for the client-server communication. Also Port-Forwarding can be done to make your PC accept request with using your global IP address. 
In the server only an admin user can edit all the configuration and provision the appliances. The project also aims to provide a secure authentication for the home visitor by powering the server with an AI engine. This will facilitate the client app for face recognition and allow only the authorized person to entry the home. Along with the regular switching ON/OFF of the home appliances, the app can monitor the amount of minutes that each home appliances are turned ON. An simple AI is provided to predict your next electricity bill. Which may not be accurate but it will show you a way to utilize the appliances properly. There by you can save your electrical expenses.


### Features:
- Simple to use.
- Light weight.
- Encrypted request(SSL).
- Ease control of your home devices using android .
- Face recognition implementation for intrusion detection system.
- All data are stored locally. User Passwords are stored encrypted.
- Auto data summary of your power consuption of devices. 
- This can send message to telegram when someone used the face recognition door lock.


[This will be updated soon]. All the configurations are stored in config.yaml file
## Client Apps:
Currently there is no official client app avaiable. Soon we will have a client app for Android devices that will work with this specific app.

## Help
### System Requirements:
#### Hardware:
- PC with 2 GB RAM (Or Raspberry Pi).
- A LAN Network.
- Arudino UNO / MEGA.

#### Software and Dependencies:
(Operating Systems supported)
- Windows 10 (Not yet tested)
- Linux (Debian and Arch based Distros)


Note: 
- This is tested well on Manjaro Linux(Arch based distro) and on Raspbain OS (Debian Based distro for Raspberry Pi).It is recommended to use Linux for using the server side script.
- It can run on other linux platforms as well. But check out your Linux distribution repository for dependencies.  

Needed Dependencies:
- Windows Dependencies
You need to have chocolately installed.
  Steps:
  1. Copy this to a powershell (Run it as administrator) :

    ```
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

    ```
  2.  Install Dependencies:
    ```choco install  python openssl```

- Linux Dependencies:
   - Manjaro/Arch Linux: 
     ``` sudo pacman -S openssl python autoconf pkgconf gcc flex patch make guile m4 cmake python-pip automake```
   - Debian/Ubuntu
     ```sudo apt install python openssl build-essentials```
  Note: 
  - If you are using Arch Linux you can use the setup_arch_linux.sh script instead.
  - If you are using Debian, you can setup_debian_linux.sh script instead.

Python Dependencies (Common for Windows/Linux):
It is recommended to create a virtual environment dedicated to this app. To create a virutal enviromemnt type:

```python -m venv venv```
Then install the python dependencies with the following command: 
```./venv/bin/python -m pip install -r requirments.txt```


Note this can take time so be patient while installing.

## SLL file generation:
follow the command sequence and type the necessary information to generate a dummy ssl certificate for your home usage:
```
openssl req -new -key server.key -out server.csr
openssl rsa -in server.key.org -out server.key
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
```
## How to run:
First setup the configurations by reading the configure section. After that run ```init.py```. If you want you can setup mod_wsgi and put the app files there. A setup Script for Debian and Arch based distributions is created. But it is a WIP script. It will be enchanced soon so that you can use a single script to add home devices, manage users and do other adminstrative things. A client app is being desiged for Android devices.

## Configuration
### Basic app configuration:
The configuration of app is stored in ``config.yaml``. It's content will look like this: 
```
# Arduino Serial Port Configuration. Execute arduino.py to find the port.
# For Windows the serial port starts at COMXX for linux it is /dev/ttyACMX.

arduinoport: /dev/ttyACM0

# Connection configuration.
# Use HTTPS  true will enable encrypted request.
use-https:  True  

# Port: changes the port number of the server.
# if port number is default is:
# 1) if it is https it uses 443 port.
# 2) if it is http it use 80 port.
port: 5000
# if You are using port number 80 or 443, you must launch the script with root privileges.
# Server Start Time: This Value is not to be modified at any cost. If you modify this data will be inconsistent.
server_start_time: 2021-02-10 14:28:13.387750
```

The configuration is done by editing the value.

To scan arduino ports run:
```python arduion.py```

### Adding home appliances (devices) and User:

#### Home appliances.

  - The list of home appliances or devices connected to arduino is listed in ``devices.yaml``.
  - Use ``store.py`` to edit the list of home appliances or devices connected to arduino. you need to type ``python store.py -a`` and type the information of the appliance.

#### User:

  - The list of users are listed in ``users.yaml``.
  - The default user is admin with the password admin.
  - Use ``store.py`` to edit the list of users that needs admin panel acess. you need to type ``python store.py -u`` and type   the information of the appliance. Soon multiple home user feature will be implemented.

##  Configure Telegram-Send 
Telegram-send is a library used by this project to send messages when some one acces your Face recongition. Face-recogntion is  can be used to unlock doors connected with the arudino(Which will be available in the release version.)

Steps to configure:

1. Go to the link https://telegram.me/BotFather (you need to have telegram installed).
2. Configure your telegram bot using telegram BotFather. (Follow the instructions)
3. Type the command ```telegram-send --configure```.
4. Type the API key given by the BotFather.
5. Enter the password to the bot that you created using BotFather.

There you go you have configured your telegram-send.

